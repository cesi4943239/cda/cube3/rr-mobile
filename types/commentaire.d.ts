export interface Commentaire {
    id: number,
    contenu: string,
    idRessource: number,
    utilisateur: {
        firstName: string,
        lastName: string,
    }
    estActif: boolean,
    reponseCommentaires: Array<Commentaire>
}