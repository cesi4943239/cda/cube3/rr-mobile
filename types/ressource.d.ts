export interface Ressource {
    id: number,
    title: string,
    creationDate: string,
    content?: string,
    author: Author,
    status: Status,
    resourceCategory: ResourceCategory,
    resourceType: RessourceType,
    relationTypes: Array<RelationTypes>,
    likeCount: number,
    savedCount: number,
    exploitedCount: number,
    nonExploitedCount: number
}

interface Author {
    firstname: string,
    lastname: string,
    email: string,
}

interface Status {
    code: string,
    caption: string
}
interface ResourceCategory extends Status {}
interface RessourceType extends Status {}
interface RelationTypes extends Status {}