export type User = {
    id: number,
    lastName: string,
    firstname: string,
    mail: string,
    isActive: boolean,
    role: number,
    ZIPCode: string
}