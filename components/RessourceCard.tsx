import React, { useState } from "react";
import { Button, Card, Text } from "react-native-paper";
import { Ressource } from "../types/ressource";
import { StyleSheet, View } from "react-native";
import ShowRessource from "./ShowRessource";
import { JsxElement } from "typescript";


export default function RessourceCard({ ressource, setSelected, selected }: { ressource: Ressource, setSelected: any, selected: number|undefined }) {
    const [ showRessource, setShowRessource ] = useState<Boolean>(false);

    const setRessourceActive = (ressource?: Ressource) => {
        if (showRessource || ressource == undefined) {
            setShowRessource(false);
            setSelected(undefined)
            return
        }
        setShowRessource(true);
        setSelected(ressource?.id)
    }

    return selected == undefined
        ? (
            <View>
                <Card style={style.card}>
                    <Card.Title title={ressource.title} />
                    <Card.Content>
                        <Text>{ressource.author.firstname} {ressource.author.lastname}</Text>
                    </Card.Content>
                    <Card.Actions>
                        <Button textColor="#000" onPress={() => setRessourceActive(ressource)}>Visualiser</Button>
                    </Card.Actions>
                </Card>
            </View>
        )
        : <ShowRessource lastRessource={ressource} disableRessource={setRessourceActive} />
}

const style = StyleSheet.create({
    card: {
        marginTop: 8,
        marginBottom: 8,
        marginLeft: 8,
        marginRight: 8
    }
})