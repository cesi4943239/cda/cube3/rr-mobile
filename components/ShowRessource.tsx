import React, { useContext, useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";
import { Button, IconButton, Snackbar, Text } from "react-native-paper";
import { Ressource } from "../types/ressource";
import apiCall from "../lib/apiCall";
import dayjs from "dayjs"
import localizedFormat from "dayjs/esm/plugin/localizedFormat"
import fr from "dayjs/locale/fr"
import { AuthContext } from "../context/AuthProvider";
import Commentaires from "./Commentaires";


export default function ShowRessource({ lastRessource, disableRessource }: { lastRessource: Ressource, disableRessource: any }) {
    dayjs.extend(localizedFormat)
    dayjs.locale(fr)

    const { userInfos } = useContext(AuthContext);
    const [ iconFav, setIconFav ] = useState<string>("heart-outline");
    const [ visibleSnackBar, setVisibleSnackBar ] = React.useState(false);
    const [ nbLikes, setNbLikes ] = useState<number>(0);

    const love = () => {
        if (userInfos) {
            if (iconFav == "heart-outline") {
                setIconFav("heart");
                setNbLikes(nbLikes + 1);
            } else {
                setIconFav("heart-outline")
                setNbLikes(nbLikes - 1);
            }
            return;
        }
        setVisibleSnackBar(true)

    }
    
    const [ ressource, setRessource ] = useState<Ressource>();
    useEffect(() => {
        const fetch = async (idRessource: number) => {
            const res = await apiCall({
                method: "GET",
                url: `/resources/${idRessource}`
            });
            setRessource(res.data);
            setNbLikes(res.data.likeCount);
        }
        fetch(lastRessource.id);
    }, [lastRessource])

    return (
        ressource 
        ? (
            <View style={style.main}>
                <View>
                    <View style={style.header}>
                        <IconButton icon={"arrow-left"} onPress={() => disableRessource()} />
                        <Text style={{ fontSize: 34 }}>{ressource.title}</Text>
                    </View>
                    <View style={style.btn}>
                        <View style={style.subBtn}>
                            <IconButton icon={iconFav} onPress={() => love()} iconColor="red" /> 
                            <Text style={style.txtBtn}>{nbLikes}</Text>
                        </View>
                        <IconButton icon={"share-variant-outline"} />
                    </View>
                    <View style={style.description}>
                        <Text>{dayjs(ressource.creationDate).format("LL")}</Text>
                        <Text style={style.content}>{ressource.content}</Text>
                        <Text style={style.author}>Publié par {ressource.author.firstname} {ressource.author.lastname}</Text>
                    </View>
                </View>
                <View style={style.viewCommentaire}>
                    <Commentaires idRessouce={ressource.id} />
                </View>
                <Snackbar visible={visibleSnackBar} onDismiss={() => setVisibleSnackBar(false)} style={style.snackbar}>
                    <Text>Vous devez être connecter pour aimer une publicaton.</Text>
                </Snackbar>
            </View>
            
        )
        : <Text>Chargement...</Text>
    )
}

const style = StyleSheet.create({
    main: {
        height: "100%"
    },
    header: {
        display: "flex",
        flexDirection: "row",
        gap: 4,
        alignItems: "center"
    },
    btn: {
        display: "flex",
        flexDirection: "row",
        gap: 4,
        alignItems: "center",
        justifyContent: "space-evenly"
    },
    subBtn: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
    txtBtn: {
        fontSize: 10,
        marginTop: -15
    },
    description: {
        margin: 16,
    },
    content: {
        marginTop: 12,
        padding: 12,
        backgroundColor: "white",
    },
    author: {
        marginTop: 12,
        textAlign: "right"
    },
    snackbar: {
        width: "100%",
        backgroundColor: "#fff"
    },
    viewCommentaire: {
        marginTop: 12,
        padding: 12,
        display: "flex",
        flexDirection: "column",
        gap: 16
    }
})