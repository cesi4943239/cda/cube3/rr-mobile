import * as React from 'react';
import 'react-native-gesture-handler';
import { StyleSheet, View } from 'react-native';


import Profile from '../screen/Profile';
import Magasine from '../screen/Magasine';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { TextInput } from 'react-native-gesture-handler';
import Home from '../screen/Home';


const Drawer = createDrawerNavigator();

function MyDrawer() {
  return (
    <NavigationContainer >
    <Drawer.Navigator initialRouteName="Catalogue" screenOptions={{headerTitle : ''}}>
      <Drawer.Screen name="Catalogue" component={Home} />
      <Drawer.Screen name="Recherche" component={Magasine} />
      <Drawer.Screen name="Profile" component={Profile} />
    </Drawer.Navigator>
    </NavigationContainer>
  );
}
    export default MyDrawer;

     const styles = StyleSheet.create({
         container: {
           flex: 1,
           display: 'flex',
           justifyContent: 'flex-end',
           backgroundColor: '#fff',
           padding: 5,
           margin: 0,
           height: '100%',
           width: 190,
         },
     });