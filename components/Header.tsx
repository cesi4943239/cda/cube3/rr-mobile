import React from "react";
import { View, Image, StyleSheet } from "react-native";
import MyDrawer from "./Drawer";
const ImageGouv = require('../assets/Gouvernement_RVB.png') ;
const uriImageGouv = Image.resolveAssetSource(ImageGouv).uri;
const ImageRessource = require('../assets/RessourcesRelationnelles.png');
const uriImageRessource = Image.resolveAssetSource(ImageRessource).uri;
export default function Header({ }: { }) {
    return (

        <View style={styles.header}>
                
                <Image
                    source = {{
                        uri : uriImageGouv
                    }}
                    style = {styles.imageGouv}
                    /> 
                <Image
                    source = {{
                    uri : uriImageRessource
                    }}
                    style = {styles.imageRessource}
                />
               
        </View>

    )
}

const styles = StyleSheet.create({
    header : {
        //flex: 1,
        display: 'flex',
        flexDirection: 'row',
        //justifyContent: 'space-between',
        height : 100,
        alignItems : 'stretch'
    },
    imageGouv : {
        width: 170,
        height :'100%',
        objectFit: 'contain',
        
    },
    imageRessource : {
        width: 110,
        height : '100%',
        objectFit: 'contain',
       
    },
    drawer : {
        objectFit: 'contain',
    }
})