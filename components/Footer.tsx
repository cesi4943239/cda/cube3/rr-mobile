import React from "react";
import { Tab, TabView, Text } from "@rneui/themed";
import { StyleProp, StyleSheet, StyleSheetProperties } from "react-native";

export default function Footer({ page, setPage }: { page: number, setPage: any }) {
console.log(page)
    return (
        <>
            <Tab
            value={page}
            onChange={(e) => setPage(e)}
            indicatorStyle={styles.tabActive}
            style={styles.tab}
            variant="primary"
            >
            <Tab.Item
                title="Accueil"
                titleStyle={{ fontSize: 12 }}
                containerStyle={{ backgroundColor: "transparent" }}
                icon={{ name: 'home', type: 'ant-design', color: 'white' }}
            />
            <Tab.Item
                title="favorite"
                titleStyle={{ fontSize: 12 }}
                containerStyle={{ backgroundColor: "transparent" }}
                icon={{ name: 'book', type: 'ant-design', color: 'white' }}
            />
            <Tab.Item
                title="cart"
                titleStyle={{ fontSize: 12 }}
                containerStyle={(isActive) => ({ backgroundColor: "transparent" })}
                icon={{ name: 'user', type: 'ant-design', color: 'white' }}
            />
            </Tab>
        </>
    )
}

const styles = StyleSheet.create({
    tab: {
        borderRadius: 9999,
        shadowColor: "rgba(0, 0, 0, 0.5)",
        shadowOffset: {
            width: 5, 
            height: 4
        },
    },
    tabActive: {
        backgroundColor: 'white',
        height: 3,
        borderRadius: 99999,
    }
})