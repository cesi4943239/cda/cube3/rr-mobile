import { useContext, useEffect, useState } from "react";
import { Commentaire } from "../types/commentaire";
import apiCall from "../lib/apiCall";
import React from "react";
import { Button, Text, TextInput } from "react-native-paper";
import { StyleSheet, View } from "react-native";
import { AuthContext } from "../context/AuthProvider";

export default function Commentaires({ idRessouce }: { idRessouce: number }) {
    const [ commentaires, setCommentaires ] = useState<Array<Commentaire>>();
    const [ newCommentaires, setNewCommentaires ] = useState<string>("");

    const { userInfos } = useContext(AuthContext);

    useEffect(() => {
        const fetch = async (idRessource: number) => {
            const res = await apiCall({
                method: "GET",
                url: `/comment/${idRessource}`
            });
            setCommentaires(res.data);
        }

        fetch(idRessouce);
    }, [])

    return (
        <>
            {userInfos
                ? (
                    <View style={{ display: "flex", flexDirection: "column", alignItems: "flex-end" }}>
                        <Text style={{ width: "100%" }}>Laissez votre commentaire ! </Text>
                        <TextInput
                            style={{ width: "100%" }}
                            multiline={true}
                            numberOfLines={8}
                            onChangeText={(text) => setNewCommentaires(text)}
                            value={newCommentaires}/>
                        <Button>Commenté !</Button>
                    </View>
                )
                : ""
            }
            {commentaires && commentaires.length 
                ? (
                    commentaires.map(commentaire => {
                        return commentaire.estActif 
                            ?  (
                                <View style={style.mainView}>
                                    <View>
                                        <Text style={style.author}>{commentaire.utilisateur.firstName} {commentaire.utilisateur.lastName} :</Text>
                                        <Text>{commentaire.contenu}</Text>
                                    </View>
                                    {commentaire.reponseCommentaires 
                                        ? 
                                        commentaire.reponseCommentaires.map(res => (
                                            <View style={style.response}>
                                                <Text style={style.author}>{res.utilisateur.firstName} {res.utilisateur.lastName} :</Text>
                                                <Text>{res.contenu}</Text>
                                            </View>
                                        ))
                                        : ""
                                    }
                                </View>
                            )
                            : ""
                    })
                )
                : commentaires?.length == 0 
                    ? ( <Text>Aucun commentaire disponible.</Text> )
                    : ( <Text>Chargement...</Text> )
        }
    </>
    )
}

const style = StyleSheet.create({
    mainView: {
        display: "flex",
        flexDirection: "column",
        gap: 16,
    },
    author: {
        fontWeight: "bold",
        fontSize: 16,
    },
    response: {
        borderTopColor: "#000", 
        borderTopWidth: 2, 
        marginHorizontal: 8, 
        paddingHorizontal: 8,
        paddingTop: 8
    }
})