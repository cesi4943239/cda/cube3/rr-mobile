import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import Footer from './components/Footer';
import { TabView } from '@rneui/themed';
import Home from './screen/Home';
import Magasine from './screen/Magasine';
import Profile from './screen/Profile';
import AuthProvider from './context/AuthProvider';
import Header from './components/Header';
import MyDrawer from './components/Drawer';
import { SafeAreaProvider } from 'react-native-safe-area-context';

export default function App() {
  const [ page, setPage ] = React.useState(0);
  return (
    <>
    <AuthProvider>
      <SafeAreaView style={styles.container}>
        <StatusBar style='light' />
        <Header></Header>
        <MyDrawer></MyDrawer>
      </SafeAreaView>
      </AuthProvider>
    </>

    
  );
}

const styles = StyleSheet.create({
  header: {
    height: 200,
    zIndex: 5,
    elevation: 5,
  },
  container: {
    flex: 1,
    flexGrow: 1,
    height: "100%",
   // backgroundColor: "#000"
     backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  footer: {
    margin: 16,
    shadowColor: "#000"
  }
});
