import React, { useContext, useState } from "react";
import { TabView, } from "@rneui/themed";
import Login from "./Profile/Login";
import { AuthContext } from "../context/AuthProvider";
import { StyleSheet, View } from "react-native";
import Register from "./Profile/Registrer";
import { Text } from "react-native-paper";
import IsConnected from "./Profile/IsConnected";

export default function Profile() {
    const { userInfos } = useContext(AuthContext);
    const [ isRegister, setIsRegister ] = useState(false);

    return (
        <TabView.Item key={"manageProfile"} style={styles.tabView}>
            {userInfos 
                ?   (
                    <IsConnected />
                )
                : (
                    <>
                        <Text style={styles.h1}>Connexion</Text>
                        <Login  key={"formulaireLogin"} isRegister={isRegister} setIsRegister={setIsRegister} styles={styles} />
                        
                        <View key={"viewLoginText"} style={{paddingLeft: 32, paddingRight: 32}}>
                            <Text key={"textViewLogin"} variant="bodyLarge">Vous { !isRegister ? "n'avez pas encore de" : "possedez un" } compte ?</Text>
                        </View>

                        <Register key={"formulaireCreation"} isRegister={isRegister} setIsRegister={setIsRegister} stylesForm={styles} />
                    </>
                )
            }
        </TabView.Item>
    )
}

const styles = StyleSheet.create({
    tabView: {
        backgroundColor: 'white',
        width: '100%',
        paddingLeft: 16,
        paddingRight: 16,
        height: "100%",
        display: "flex",
        flexDirection: "column",
        gap: 16
    },
    h1: {
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 24
    },
    view: {
        padding: 16,
        backgroundColor: '#f3f8f8'
    },
    inputPseudo: {
        width: "100%",
        height: 40
    }
})