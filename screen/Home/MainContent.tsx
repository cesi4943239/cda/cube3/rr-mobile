import React, { useEffect, useState } from "react";
import RessourceCard from "../../components/RessourceCard";
import apiCall from "../../lib/apiCall";
import { Ressource } from "../../types/ressource";
import { Text } from "react-native-paper";
import { ScrollView, StyleSheet } from "react-native";

let startAncestor;

export default function MainContent() {
    const [ selected, setSelected ] = useState<number|undefined>(undefined);
    const [ ressources, setRessources ] = useState<Array<Ressource>>();
    useEffect(() => {
        const fetch = async () => {
            const ressourcesAPI = await apiCall({method: "GET", url: "/resources"})
            setRessources(ressourcesAPI.data)
        }
        fetch();
    }, [])

    return (
        ressources && ressources.length
            ? ( 
                <ScrollView style={styles.scrollView} >
                    {ressources.map((ressource: Ressource) => {
                        return selected == undefined || selected == ressource.id
                            ? <RessourceCard key={ressource.id} ressource={ressource} setSelected={setSelected} selected={selected} />
                            : ""
                    })}
                </ScrollView>
            )
            : ressources?.length == 0 
                ? ( <Text>Aucune ressource n'est disponible.</Text> )
                : ( <Text>Chargement...</Text> )
    )
}


const styles = StyleSheet.create({
    scrollView: {
        display: "flex",
        flexDirection: "column",
        gap: 24,
    }
})