import { useContext, useState } from "react";
import React from "react";
import { StyleSheet, StyleSheetProperties, View } from "react-native";
import { Button, Input } from "@rneui/base";
import { AuthContext } from "../../context/AuthProvider";
import { HelperText, TextInput } from "react-native-paper";
import apiCall from "../../lib/apiCall";

export default function Login({ isRegister, setIsRegister, styles }: { isRegister: boolean, setIsRegister: any, styles: any}) {
    const { login }: { login: any } = useContext(AuthContext);

    const [ mail, setMail ] = useState<string>("");
    const [ password, setPassword ] = useState<string>("");

    // Gestion de l'affichage des erreurs
    const [ mailError, setMailError ] = useState<string>("");
    const [ passwordError, setPasswordError ] = useState<string>("");

    // Gestion de l'affichage du mot de passe
    const [ isInvisiblePsw, setIsInvisiblePsw ] = useState<boolean>(true);
    const [ iconPassword, setIconPassword ] = useState<string>("eye-outline");

    const handleInput = (setValue: any, value: string, setError: any) => {
        setValue(value);
        setError();
    }

    const handlePassword = () => {
        setIsInvisiblePsw(!isInvisiblePsw)
        setIconPassword(!isInvisiblePsw ? "eye-outline" : "eye-off-outline")
    }

    // Check before auth
    const checkLogin = () => {
        if (!mail.trim()) setMailError("Le nom d'utilisateur n'est pas valide");
        if (!password.trim()) setPasswordError("Le mot de passe n'est pas valide");

        if (mailError || passwordError) return;

        const body = {
            mail: mail,
            password: password
        }

        apiCall({
            url: "/login", 
            body: body,
            method: "POST"
        }).then((res: any) => {
            if (res.errors) {
                switch (res.errors[0]) {
                    case "Invalid email address":
                        setMailError("L'email n'est pas valide");
                        break;
                    default: 
                        if (res.errors == "Resource not found") {
                            setMailError("Aucun utilisateur associé");
                            return
                        } else if (res.errors == "Unauthorized") {
                            setPasswordError("Mot de passe incorrect");
                            return;
                        }
                        console.log(res.errors[0])
                }
                return
            }
            console.log(res)
            login(res.data.token)
        }).catch((err) => { throw new Error(err) })
    }

    return (
        <View style={{ ...styles.view, display: isRegister ? "none" : "flex"}}>
            <TextInput 
              label={"Nom d'utilisateur"}
              placeholder="Saisir le nom d'utilisateur"
              onChangeText={text => handleInput(setMail, text, setMailError)}
              error={mailError ? true : false}
              keyboardType="email-address"
              left={<TextInput.Icon icon="account-outline" />}
            />
            <HelperText type={"error"} >{mailError}</HelperText>

            <TextInput 
              label={"Mot de passe"}
              placeholder="Saisir le mot de passe"
              onChangeText={text => handleInput(setPassword, text, setPasswordError)}
              error={passwordError ? true : false}
              secureTextEntry={isInvisiblePsw}
              left={<TextInput.Icon icon="lock-outline" />}
              right={<TextInput.Icon icon={iconPassword} onPress={() => handlePassword()} />}
            />
            <HelperText type={"error"} >{passwordError}</HelperText>
            <Button onPress={() => checkLogin()} title={"Se connecter"}/>
        </View>
    )
}