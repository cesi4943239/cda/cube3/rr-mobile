import React, { useContext } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { Button, Dialog, Snackbar, Text } from "react-native-paper";
import RessourceCard from "../../components/RessourceCard";
import { AuthContext } from "../../context/AuthProvider";
import { Ressource } from "../../types/ressource";

const RessourcesExemple: Array<Ressource> = [
    {
        id: 1,
        title: "Crinis quo cattus.",
        creationDate: "2024-02-23T12:24:46.000Z",
        author: {
            firstname: "Maxwell",
            lastname: "Jacobs",
            email: "1391Duncan.Macejkovic@yahoo.com",
        },
        status: {
            code: "BROUILLON",
            caption: "Brouillon",
        },
        resourceCategory: {
            code: "COMM",
            caption: "Communication",
        },
        resourceType: {
            code: "DEFI",
            caption: "Carte défi",
        },
        relationTypes: [
            {
                code: "FAMILLE",
                caption: "Famille : enfants / parents / fratrie",
            },
        ],
        likeCount: 0,
        savedCount: 0,
        exploitedCount: 1,
        nonExploitedCount: 1,
    },
    {
        id: 2,
        title: "Adipiscor.",
        creationDate: "2024-03-01T18:19:41.000Z",
        author: {
            firstname: "Fern",
            lastname: "Reichert",
            email: "86735Brett.Kilback42@gmail.com",
        },
        status: {
            code: "ATTENTE",
            caption: "En attente",
        },
        resourceCategory: {
            code: "INTEMOT",
            caption: "Intelligence émotionnelle",
        },
        resourceType: {
            code: "COURS",
            caption: "Cours au format PDF",
        },
        relationTypes: [
            {
                code: "INCONNU",
                caption: "Inconnus",
            },
        ],
        likeCount: 0,
        savedCount: 1,
        exploitedCount: 0,
        nonExploitedCount: 0,
    },
    {
        id: 3,
        title: "Adflicto defaeco.",
        creationDate: "2024-02-24T10:42:18.000Z",
        author: {
            firstname: "Maudie",
            lastname: "Denesik-Rohan",
            email: "74108Edison_Franecki@yahoo.com",
        },
        status: {
            code: "PUBLIC",
            caption: "Publique",
        },
        resourceCategory: {
            code: "QUALVIE",
            caption: "Qualité de vie",
        },
        resourceType: {
            code: "ACTIVITE",
            caption: "Activité / Jeu à réaliser",
        },
        relationTypes: [
            {
                code: "PROFESSIONNEL",
                caption: "Professionnelle : collègues, collaborateurs et managers",
            },
        ],
        likeCount: 0,
        savedCount: 1,
        exploitedCount: 1,
        nonExploitedCount: 0,
    },
    {
        id: 4,
        title: "Utrimque allatus.",
        creationDate: "2024-02-13T06:47:53.000Z",
        author: {
            firstname: "Brady",
            lastname: "Botsford",
            email: "50781Titus12@yahoo.com",
        },
        status: {
            code: "DESACTIVE",
            caption: "Désactivée",
        },
        resourceCategory: {
            code: "SANTEPSY",
            caption: "Santé psychique",
        },
        resourceType: {
            code: "JEUVIDEO",
            caption: "Jeu en ligne",
        },
        relationTypes: [
            {
                code: "CONJOINT",
                caption: "Conjoint",
            },
        ],
        likeCount: 0,
        savedCount: 1,
        exploitedCount: 1,
        nonExploitedCount: 1,
    },
    {
        id: 5,
        title: "Dedico commodo.",
        creationDate: "2024-02-11T03:54:21.000Z",
        author: {
            firstname: "Camilla",
            lastname: "Ward",
            email: "20273Abdul_Beatty48@yahoo.com",
        },
        status: {
            code: "DESACTIVE",
            caption: "Désactivée",
        },
        resourceCategory: {
            code: "RECHSENS",
            caption: "Recherche de sens",
        },
        resourceType: {
            code: "DEFI",
            caption: "Carte défi",
        },
        relationTypes: [
            {
                code: "SOI",
                caption: "Soi",
            },
        ],
        likeCount: 1,
        savedCount: 0,
        exploitedCount: 0,
        nonExploitedCount: 0,
    },
];

export default function IsConnected() {
    const { logout, userInfos } = useContext(AuthContext);

    const [visible, setVisible] = React.useState(false);
    const [visibleSnackBar, setVisibleSnackBar] = React.useState(false);

    const deleteAccount = () => {
        setVisibleSnackBar(true);
        setVisible(false);
    };
    return (
        <>
            <ScrollView>
                <View>
                    <Text variant="headlineLarge">Mon compte</Text>
                </View>
                <View style={styles.viewContainer}>
                    <Button>Modifier mes informations</Button>
                    <ScrollView style={{ width: "100%", padding: 4 }}>
                        <View style={styles.scrollView}>
                            {RessourcesExemple.map((ressource) => (
                                <RessourceCard ressource={ressource} key={ressource.id} setSelected={undefined} selected={undefined} />
                            ))}
                        </View>
                    </ScrollView>
                    <Button onPress={() => logout()}>Se déconnecter</Button>
                    <Button onPress={() => setVisible(true)} mode={"contained-tonal"}>
                        Supprimer mon compte
                    </Button>
                </View>
            </ScrollView>
            <Dialog visible={visible} onDismiss={() => setVisible(false)}>
                <Dialog.Title>Supprimer le compte</Dialog.Title>
                <Dialog.Content>
                    <Text variant="bodyMedium">
                        Êtes-vous sûr de vouloir supprimer le compte ?
                    </Text>
                </Dialog.Content>
                <Dialog.Actions>
                    <Button
                        onPress={() => deleteAccount()}
                        buttonColor="rgb(186, 26, 26)"
                        mode="contained"
                    >
                        Confirmer
                    </Button>
                    <Button onPress={() => setVisible(false)}>Annuler</Button>
                </Dialog.Actions>
            </Dialog>
            <Snackbar
                visible={visibleSnackBar}
                onDismiss={() => setVisibleSnackBar(false)}
                style={styles.snackbar}
            >
                <Text>
                    Suppression du compte... (Cette action arrivera prochainent)
                </Text>
            </Snackbar>
        </>
    );
}

const styles = StyleSheet.create({
    viewContainer: {
        flex: 1,
        gap: 4,
        alignItems: "center",
        justifyContent: "center",
        width: "100%",
    },
    scrollView: {
        width: "100%",
        flex: 1,
        gap: 4,
    },
    snackbar: {
        width: "100%",
        backgroundColor: "#fff",
    },
});
