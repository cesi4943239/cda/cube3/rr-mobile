import React, { useContext, useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { Button, Checkbox, HelperText, Text, TextInput } from "react-native-paper";
import apiCall from "../../lib/apiCall";
import { AuthContext } from "../../context/AuthProvider";

export default function Register({ isRegister, setIsRegister, stylesForm }: { isRegister: boolean, setIsRegister: any, stylesForm: any}) {
    const { login } = useContext(AuthContext);

    // Mémorisation des champs
    const [ firstname, setFirstname ] = useState<string>("");
    const [ lastname, setLastname ] = useState<string>("");
    const [ zipCode, setZipCode ] = useState<string>("");
    const [ mail, setMail ] = useState<string>("");
    const [ password, setPassword ] = useState<string>("");
    const [ passwordRepeate, setPasswordRepeate ] = useState<string>("");
    const [ accept, setAccept ] = useState<boolean>(false);

    // Mémorisation des messages d'erreurs 
    const [ firstnameError, setFirstnameError ] = useState<string>("")
    const [ lastnameError, setLastnameError ] = useState<string>("")
    const [ zipCodeError, setZipCodeError ] = useState<string>("")
    const [ mailError, setMailError ] = useState<string>("")
    const [ passwordError, setPasswordError ] = useState<string>("")
    const [ passwordRepeateError, setPasswordRepeateError ] = useState<string>("")
    const [ acceptError, setAcceptError ] = useState<string>("")

    /**
     * Enregistre dans la variable **setVariable** la valeur de **value** lors d'une saisie
     * @param setValue Fonction de modification d'un champ
     * @param value Valeur à modifier de `setValue`
     * @param error Réinitialise la variable Erreur associé à `setValue`
     */
    const handleInput = (setValue: any, value: any, error: any) => {
        error("");
        setValue(value);
    }

    /**
     * Permet d'afficher une erreur à l'utilisateur
     * @param setError Fonction de gestion d'une erreur
     * @param error Message à affiché dans l'erreur
     */
    const handleError = (setError: any, error: string) => {
        setError(error)
    }

    /**
     * Envoie le formulaire d'autehtification.  
     * Si l'un des champs n'est pas remplis / conforme, il retourne une erreur.
     */
    const handleSubmit = () => {
        if (firstname.trim() == "") handleError(setFirstnameError, "Veuillez saisir un prénom valide")
        if (lastname.trim() == "") handleError(setLastnameError, "Veuillez saisir un nom valide")
        if (zipCode.trim.length << 5) handleError(setZipCodeError, "Veuillez saisir un code postal valide")
        if (mail.trim() == "") handleError(setMailError, "Veuillez saisir un email valide")
        if (password.trim() == "") handleError(setPasswordError, "Veuillez saisir un mot de passe valide")
        if (passwordRepeate.trim() == "") handleError(setPasswordRepeateError, "Veuillez saisir un répeter votre mot de passe")
        if (!accept) setAcceptError("Veuillez accepter les conditions d'utilisation");

        if (firstnameError || lastnameError || zipCodeError || mailError || passwordError || passwordRepeateError || acceptError) {
            return;
        }

        if (password != passwordRepeate) {
            setPasswordError("Les mots de passe ne correspondent pas.");
            return
        } 

        const body = {
            lastName: lastname.trim(),
            firstName: firstname.trim(),
            mail: mail.trim(),
            password: password,
            ZIPCode: zipCode
        };

        apiCall({
            url: "/register",
            body: body,
            method: "POST"
        }).then((res) => {
            if (res.errors) {
                switch (res.errors[0]) {
                    case "Invalid email address":
                        setMailError("L'email n'est pas valide");
                        break;
                    default: 
                        if (res.errors == "Conflit occured") {
                            setMailError("L'utilisateur existe déjà");
                            return
                        }
                        console.log(res.errors[0])
                }
            }
            login(res.data.token)
        }).catch((err) => console.error(err))
    }

    const inputs: Array<any> = [
        {
            key: 1,
            label: "Prénom",
            placeholder: "Saisisez votre prénom",
            value: firstname,
            setValue: setFirstname,
            setError: setFirstnameError,
            error: firstnameError,
            icon: "account-outline"
        },
        {
            key: 2,
            label: "Nom",
            placeholder: "Saisisez votre nom",
            value: lastname,
            setValue: setLastname,
            setError: setLastnameError,
            error: lastnameError,
            icon: "account-outline"
        },
        {
            key: 3,
            label: "Code postal",
            placeholder: "Saisisez votre code postal",
            keybordType: "numeric",
            value: zipCode,
            setValue: setZipCode,
            setError: setZipCodeError,
            error: zipCodeError,
            icon: "account-outline",
            maxLength: 5,
        },
        {
            key: 4,
            label: "Email",
            placeholder: "Saisisez votre email",
            keybordType: "email-address",
            value: mail,
            setValue: setMail,
            setError: setMailError,
            error: mailError,
            icon: "account-outline"
        },
        {
            key: 5,
            label: "Mot de passe",
            placeholder: "Saisisez votre mot de passe",
            value: password,
            setValue: setPassword,
            setError: setPasswordError,
            error: passwordError,
            icon: "account-outline",
            isSecure: true,
        },
        {
            key: 6,
            label: "Répeter le mot de passe",
            placeholder: "Répetez votre mot de passe",
            value: passwordRepeate,
            setValue: setPasswordRepeate,
            setError: setPasswordRepeateError,
            error: passwordRepeateError,
            icon: "account-outline",
            isSecure: true,
        }
    ]

    const changeOnglet = () => {
        setIsRegister(!isRegister)
        inputs.forEach(input => handleInput(input.setValue, "", input.setError))
    }

    const lbCondition = "J'ateste avoir lu et accepte les Conditions générals d'utilisation ainsi que la politique de confidentialité";

    return (
        <View key={"ViewRegister"} style={styles.view}>
            <Button key={"ViewBtnConnexion"} onPress={() => changeOnglet()}>{ !isRegister ? "Créer un compte" : "Se connecter" }</Button>
            <ScrollView key={"ScrollWiewRegister"} automaticallyAdjustKeyboardInsets style={{ display: isRegister ? "flex" : "none", ...stylesForm.view, gap: 4}}>
                {inputs.map(input => (
                    <>
                        <TextInput 
                            key={input.key}
                            label={input.label}
                            placeholder={input.placeholder}
                            value={input.value}
                            keyboardType={input.keybordType ?? "default"}
                            onChangeText={text => handleInput(input.setValue, text, input.setError)}
                            error={input.error ? true : false }
                            secureTextEntry={input.isSecure? true : false}
                            left={ <TextInput.Icon icon={input.icon} /> } 
                            maxLength={input.maxLength ?? undefined}
                        /> 
                        <HelperText key={"error" + input.key} type={"error"} >{input.error}</HelperText>
                    </>
                ))}
                <View  key={"ViewRegisterBtn"} style={styles.viewCheckbox}>
                    <Checkbox.Item key={"checkBoxCondition"} label={lbCondition} status={accept ? "checked" : "unchecked"} uncheckedColor="#000" onPress={() => handleInput(setAccept, !accept, setAcceptError)} />
                    <HelperText key={"ErrorKey"} type={"error"} >{acceptError}</HelperText>
                    <Button key={"BtnResiger"} onPress={() => handleSubmit()}>S'inscrire</Button>
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        height: "100%",
        width: "100%",
    },
    viewCheckbox: {
        flex: 1, 
        flexDirection: "column",
        paddingBottom: 16,
    },
})