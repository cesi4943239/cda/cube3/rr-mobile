import React, { useEffect, useState } from "react";
import RessourceCard from "../../components/RessourceCard";
import apiCall from "../../lib/apiCall";
import { Ressource } from "../../types/ressource";
import { Text, TextInput } from "react-native-paper";
import { ScrollView, StyleSheet } from "react-native";
import { SafeAreaView } from "react-native";
import { MultipleSelectList } from 'react-native-dropdown-select-list'

export default function MainMagasine() {
    
    const [ selected, setSelected ] = useState<number|undefined>(undefined);
    const [text, onChangeText] = React.useState('Titre');

    const [selectedddc, setSelectedddc] = React.useState([]);
  
    const datac = [
        {key:'COMM', value:'Communication'},
        {key:'CULT', value:'Cultures'},
        {key:'DEVPER', value:'Développement personnel'},
        {key:'INTEMOT', value:'Intelligence emotionnelle'},
        {key:'LOIS', value:'Loisirs'},
        {key:'PROF', value:'Monde professionnel'},
        {key:'PAREN', value:'Parentalite'},
        {key:'QUALVIE', value:'Qualité de vie'},
        {key:'RECHSENS', value:'Recherche de sens'},
        {key:'SANTEPHY', value:'Santé physique'},
        {key:'SANTEPSY', value:'Santé psychique'},
        {key:'SPIRIT', value:'Spiritualité'},
        {key:'VIEAFF', value:'Vie affective'},
    ]

    const [selectedddrs, setSelectedddrs] = React.useState([]);
  
    const datars = [
        {key:'ACTIVITE', value:'Activite'},
        {key:'ARTICLE', value:'Article'},
        {key:'DEFI', value:'Defi'},
        {key:'COURS', value:'Cours'},
        {key:'EXERCICE', value:'Exercice'},
        {key:'LECTURE', value:'Lecture'},
        {key:'JEUVIDEO', value:'Jeu video'},
        {key:'VIDEO', value:'Video'},
    ]

    const [selectedddrl, setSelectedddrl] = React.useState([]);
  
    const datarl = [
        {key:'SOI', value:'Soi'},
        {key:'CONJOINT', value:'Conjoint'},
        {key:'FAMILLE', value:'Famille'},
        {key:'PROFESSIONNEL', value:'Professionnel'},
        {key:'AMI', value:'Ami'},
        {key:'INCONNU', value:'Inconnu'},
    ]


  const [ressources, setRessources] = useState<Array<Ressource>>();
  useEffect(() => {
    const fetch = async () => {
      const ressourcesAPI = await apiCall({ method: "GET", url: "/resources" });
      setRessources(ressourcesAPI.data);
    };
    fetch();
  }, [selectedddc, selectedddrl, selectedddrs]);


  


  return (
    <>

        <SafeAreaView>
            <TextInput
                onChangeText={onChangeText}
                value={text}
            />

            <MultipleSelectList 
                placeholder="Categories"
                label="Categories"
                setSelected={(val: React.SetStateAction<never[]>) => setSelectedddc(val)} 
                data={datac} 
                save="value"
            />
            <MultipleSelectList 
                placeholder="Ressources"
                label="Ressources"
                setSelected={(val: React.SetStateAction<never[]>) => setSelectedddrs(val)} 
                data={datars} 
                save="value"
            />
            <MultipleSelectList 
                placeholder="Relation"
                label="Relation"
                setSelected={(val: React.SetStateAction<never[]>) => setSelectedddrl(val)} 
                data={datarl} 
                save="value"
            />

        </SafeAreaView> 

        {ressources && ressources.length
            ? ( 
                <ScrollView style={styles.scrollView} >
                    {ressources?.map((ressource: Ressource) => <RessourceCard key={ressource.id} ressource={ressource} setSelected={setSelected} selected={selected} />)}
                </ScrollView>
            )
            : ressources?.length == 0 
                ? ( <Text>Aucune ressource n'est disponible.</Text> )
                : ( <Text>Chargement...</Text> )
            }

    </>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    display: "flex",
    flexDirection: "column",
    gap: 24,
  },
});
