import React from "react";
import { TabView, Text } from "@rneui/themed";
import MainContent from "./Home/MainContent";

export default function Home() {
    return (
        <TabView.Item style={{ width: '100%', height: "100%" }}>
            <MainContent />
        </TabView.Item>
    )
}